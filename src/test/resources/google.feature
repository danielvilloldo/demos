Feature: See the results for a "Cucumber" search in Google

    Scenario Outline: Search <string> on Google, print the names of the first page and verify that the first link is what we spect.
        Given have Google website opened
        When search "<string>" on Google
        Then print the text of the links in the console
        And verify first link

    Examples:
        |string|
        |cucumber|
