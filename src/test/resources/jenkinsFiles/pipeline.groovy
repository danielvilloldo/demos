package jenkinsFiles;

#!groovy

node {
    try {
        stage('Clone repository') {
            //private repo
            git url: 'https://danielvilloldo@bitbucket.org/danielvilloldo/demos.git'
        }
        stage('Execute tests') {
            script {
                bat 'gradle cleanTest'
                bat 'gradle test'
            }
        }
        currentBuild.result = "SUCCESS"
    } catch (Exception e) {
        currentBuild.result = "FAILURE"
        throw e
    } finally {
        emailext (
                to: 'daniel.villoldo@alten.es',
                replyTo: 'daniel.villoldo@alten.es',
                subject: "Build report from - '${env.JOB_NAME}' ",
                body: 'The status of the build from the pipeline ' + env.JOB_NAME + ' was ' + currentBuild.result);
        if(currentBuild.result == "SUCCESS") {
            //WithCredentials
            bat 'curl --location --request POST "https://slack.com/api/chat.postMessage?channel=formacionjenkins&text=SUCCESS" --header "Authorization: Bearer xoxp-382121821795-882634093846-887904719907-3049bcf55f1a32e07277d274272b0b0f"'
        } else {
            bat 'curl --location --request POST "https://slack.com/api/chat.postMessage?channel=formacionjenkins&text=FAILURE" --header "Authorization: Bearer xoxp-382121821795-882634093846-887904719907-3049bcf55f1a32e07277d274272b0b0f"'
        }
    }
}