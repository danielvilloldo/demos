Feature: Buy 3 tickets in Renfe website

  Scenario Outline: Buy tickets (<adults> adult and <children> children) from <origin> to <destination> for <departure> and return <return> in Renfe website.
    Given opened Renfe website
    When complete origin city option with "<origin>" and destination city option with "<destination>"
    Then complete departure date with "<departure>" and return date with "<return>"
    And complete adults option with "<adults>" and children option with "<children>"
    Then click buy button
    And buy the first ticket option

    Examples:
      | origin     | destination | adults | children | departure  | return     |
      | Valladolid | Madrid      | 1      | 2        | 01/01/2020 | 06/01/2020 |