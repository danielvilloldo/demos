import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)


@CucumberOptions(
        features = {"src/test/resources/google.feature"},
        glue = {"FESteps"}
)

/*
@CucumberOptions(
        features = {"src/test/resources/renfe.feature"},
        glue = {"FESteps"}
)*/

public class TestRunner {

}
