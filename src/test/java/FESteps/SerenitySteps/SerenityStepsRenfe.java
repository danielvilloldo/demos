package FESteps.SerenitySteps;

import PageObjects.RenfeHome;
import PageObjects.RenfeResults;
import net.thucydides.core.annotations.Step;

public class SerenityStepsRenfe {
    private RenfeHome renfeHome;
    private RenfeResults renfeResults;

    @Step("Given renfe website opened")
    public void openRenfe() {
        renfeHome.openPage();
    }

    @Step("When complete origin city option with \"([^\"]*)\" and destination city option with \"([^\"]*)\"")
    public void completeCities(String city1, String city2) {
        renfeHome.completeCities("VALLADOLID", "MADRID (TODAS)");
    }

    @Step("Then complete departure date with \"([^\"]*)\" and return date with \"([^\"]*)\"")
    public void completeDates(String date1, String date2) {
        renfeHome.completeDates("01/01/2020", "07/01/2020");
    }

    @Step("And complete adults option with \"([^\"]*)\" and children option with \"([^\"]*)\"")
    public void completePeople(String nAdults, String nChildren) {
        renfeHome.completePeople("1", "2");
    }

    @Step("Then click buy button")
    public void searchTickets() {
        renfeHome.searchTickets();
    }

    @Step("And verify if departure city is what we expected")
    public void buyFirstTicket() {
        renfeResults.selectFirstTicket();
    }
}
