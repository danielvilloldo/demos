package FESteps.SerenitySteps;

import PageObjects.GoogleHome;
import PageObjects.GoogleResults;
import net.thucydides.core.annotations.Step;

public class SerenitySteps {
    private GoogleHome googleHome;
    private GoogleResults googleResults;

    @Step("Given have Google website opened")
    public void openGoogleSite(){
        googleHome.openPage();
    }

    @Step("When search \"([^\"]*)\" on Google")
    public void searchOnGoogle(String str) {
        googleHome.searchOnGoogle("cucumber");
    }

    @Step("Then print the text of the links in the console")
    public void printLinks() {
        googleResults.printLinks();
    }

    @Step("And verify first link")
    public void verifyLink() {
        googleResults.verifyLink();
    }
}
