package FESteps;

import FESteps.SerenitySteps.SerenityStepsRenfe;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class StepsDefinitionsRenfe {

    @Steps
    SerenityStepsRenfe serenityStepsRenfe;

    @Given("opened Renfe website")
    public void openRenfeWebsite() {
        serenityStepsRenfe.openRenfe();
    }

    @When("complete origin city option with \"([^\"]*)\" and destination city option with \"([^\"]*)\"")
    public void citiesComplete(String city1, String city2) {
        serenityStepsRenfe.completeCities("VALLADOLID", "MADRID (TODAS)");
    }

    @Then("complete departure date with \"([^\"]*)\" and return date with \"([^\"]*)\"")
    public void datesComplete(String date1, String date2) {
        serenityStepsRenfe.completeDates("01/01/2020", "07/01/2020");
    }

    @And("complete adults option with \"([^\"]*)\" and children option with \"([^\"]*)\"")
    public void peopleComplete(String nAdults, String nChildren) {
        serenityStepsRenfe.completePeople("1", "2");
    }

    @Then("click buy button")
    public void clickBuy() {
        serenityStepsRenfe.searchTickets();
    }

    @And("buy the first ticket option")
    public void buyTicket() {
        serenityStepsRenfe.buyFirstTicket();
    }
}
