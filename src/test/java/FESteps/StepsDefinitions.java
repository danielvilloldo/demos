package FESteps;

import FESteps.SerenitySteps.SerenitySteps;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class StepsDefinitions {

    @Steps
    SerenitySteps serenitySteps;

    @Given("^have Google website opened$")
    public void googleOpen(){
        serenitySteps.openGoogleSite();
    }

    @When("^search \"([^\"]*)\" on Google$")
    public void googleSearch(String str){
        serenitySteps.searchOnGoogle("cucumber");
    }

    @Then("^print the text of the links in the console$")
    public void printResults(){
        serenitySteps.printLinks();
    }

    @And("^verify first link$")
    public void verifyLink(){
        serenitySteps.verifyLink();
    }
}
