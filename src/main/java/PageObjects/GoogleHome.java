package PageObjects;

import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import net.serenitybdd.core.pages.PageObject;
import java.util.concurrent.TimeUnit;
import static java.time.temporal.ChronoUnit.SECONDS;

@DefaultUrl("https://www.google.es")

public class GoogleHome extends PageObject {

    @FindBy(name = "q")
    public WebElement searchGoogle;

    public GoogleHome(WebDriver driver) {
        super(driver);
    }

    public void openPage() {
        this.setImplicitTimeout(15, SECONDS);
        this.open();
        waitLoad();
    }

    public void waitLoad() {
        this.getDriver().manage().timeouts().pageLoadTimeout(15, TimeUnit.SECONDS);
    }

    public void searchOnGoogle(String str) {
        searchGoogle.sendKeys("cucumber");
        searchGoogle.submit();
    }
}
