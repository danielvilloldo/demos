package PageObjects;

import net.thucydides.core.pages.PageObject;
import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class RenfeResults extends PageObject {

    @FindBy(xpath = "/html/body/div[1]/section[2]/div/section/div[2]/div[1]/table/tbody[1]/tr[1]/td[6]/button")
    public WebElement firstTicket;
    @FindBy(xpath = "/html/body/div[1]/section[2]/div/section/div[2]/div[1]/table/tbody[1]/tr[2]/td/ul/li[3]/div/button[1]")
    public WebElement addToChart;
    @FindBy(id = "tuCompraPrecioTotal")
    public WebElement totalPriceExpected;

    public void selectFirstTicket() {
        try {
            Thread.sleep(200);
            firstTicket.click();
            Thread.sleep(200);
            addToChart.click();

            int totalPrice = Integer.parseInt(firstTicket.getText()) * 3;
            Assert.assertEquals(totalPrice+"", totalPriceExpected.getText());

        } catch (Exception e) {
            System.out.println("Exception: " + e);
        }
    }
}
