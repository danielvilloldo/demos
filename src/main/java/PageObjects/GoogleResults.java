package PageObjects;

import net.thucydides.core.pages.PageObject;
import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GoogleResults extends PageObject {

    @FindBy(className = "LC20lb")
    public List<WebElement> links;

    private static final String EXPECTED_LINK = "Cucumber | Tools & techniques that elevate teams to greatness";

    public void printLinks() {
        Logger logger = Logger.getLogger(GoogleResults.class.getName());
        for (int j = 0; j < links.size(); j++) {
            logger.log(Level.INFO, "Link " + (j+1) + ": " + links.get(j).getText());
        }
    }

    public void verifyLink() {
        String firstLink = links.get(0).getText();
        Assert.assertEquals(EXPECTED_LINK, firstLink);
    }
}
