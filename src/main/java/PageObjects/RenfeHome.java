package PageObjects;

import jxl.write.DateTime;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import static java.time.temporal.ChronoUnit.SECONDS;

@DefaultUrl("http://renfe.es")

public class RenfeHome extends PageObject {
    @FindBy(id = "IdOrigen")
    public WebElement origin;
    @FindBy(id = "IdDestino")
    public WebElement destination;
    @FindBy(id = "__fechaIdaVisual")
    public WebElement departureDay;
    @FindBy(id = "__fechaVueltaVisual")
    public WebElement returnDate;
    @FindBy(id = "__numAdultos")
    public WebElement adults;
    @FindBy(id = "__numNinos")
    public WebElement children;
    @FindBy(className = "btn_home")
    public WebElement btnBuy;

    public RenfeHome(WebDriver driver) {
        super(driver);
    }

    public void openPage() {
        this.setImplicitTimeout(15, SECONDS);
        this.getDriver().manage().window().maximize();
        this.open();
        waitLoad();
    }

    public void waitLoad() {
        this.getDriver().manage().timeouts().pageLoadTimeout(15, TimeUnit.SECONDS);
    }

    public void completeCities(String city1, String city2) {
        origin.sendKeys("VALLADOLID");
        try {
            Thread.sleep(500);
            origin.sendKeys(Keys.ENTER);
        } catch (Exception e) {
            System.out.println("Exception: " + e);
        }
        destination.sendKeys("MADRID");
        try {
            Thread.sleep(500);
            destination.sendKeys(Keys.ENTER);
        } catch (Exception e) {
            System.out.println("Exception: " + e);
        }
    }

    public void completePeople(String nAdults, String nChildren) {
        adults.clear();
        adults.sendKeys("1");
        children.clear();
        children.sendKeys("2");
    }

    public void completeDates(String date1, String date2) {
        departureDay.clear();
        Calendar todayDate = Calendar.getInstance();
        SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy");
        Date date = todayDate.getTime();
        String departureDate = format1.format(date);
        departureDay.sendKeys(departureDate);
        returnDate.clear();
        todayDate.add(Calendar.DATE, 5);
        Date dateReturn = todayDate.getTime();
        String returnDay = format1.format(dateReturn);
        returnDate.sendKeys(returnDay);
    }

    public void searchTickets() {
        btnBuy.click();
    }
}
