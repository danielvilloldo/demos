import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.ArrayList;
import java.util.List;

public class Prueba {
    public static void main(String[] args) {
        System.setProperty("webdriver.chrome.driver", "C:\\tools\\chromedriver_win32\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();

        driver.get("https://www.google.es");

        WebElement searchGoogle = driver.findElement(By.name("q"));
        searchGoogle.sendKeys("cucumber");
        searchGoogle.submit();
        List<WebElement> links = driver.findElements(By.className("LC20lb"));
        for (int j = 0; j < links.size(); j++) {
            System.out.println(links.get(j).getText());
        }
        String firstLink = links.get(0).getText();
        Assert.assertEquals("Cucumber | Tools & techniques that elevate teams to greatness", firstLink);
    }
}
